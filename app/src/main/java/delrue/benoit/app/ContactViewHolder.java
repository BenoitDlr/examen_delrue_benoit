package delrue.benoit.app;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class ContactViewHolder extends RecyclerView.ViewHolder {

    private TextView name;
    private TextView phone;
    private TextView mail;

    private ImageView photo;

    public ContactViewHolder(@NonNull View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.nomContact);
        mail = itemView.findViewById(R.id.mailContact);
        phone = itemView.findViewById(R.id.phoneContact);
        photo = itemView.findViewById(R.id.imageProfil);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, ContactDetails.class);
                intent.putExtra("individu", name.getText());
                context.startActivity(intent);
            }
        });
    }
    void display(Contact c){
        name.setText(c.getName().getFirst() + c.getName().getLast());
        mail.setText(c.getEmail());
        phone.setText(c.getPhone());
        Picasso.get().load(c.getPicture()).into(photo);
    }
}
