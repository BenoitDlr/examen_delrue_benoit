package delrue.benoit.app;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitClient {
    @GET("eilco.json")
    Call<List<Contact>> GetContact();
}
